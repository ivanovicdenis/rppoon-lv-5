﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace RPPOON___LV_5_4
{
    class Dataset : IDataset
    {
        private string filePath;
        private List<List<string>> data;
        private LogProxyDataset logProxy;
        public Dataset(string filePath)
        {
            this.filePath = filePath;
            this.data = new List<List<string>>();
            this.LoadDataFromCSV();
            logProxy = new LogProxyDataset();
        }
        private void LoadDataFromCSV()
        {
            string[] lines = System.IO.File.ReadAllLines(this.filePath);
            foreach (string line in lines)
            {
                List<string> row = new List<string>(line.Split(','));
                data.Add(row);
            }
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            logProxy.LogReadAttempt(filePath);
            return new ReadOnlyCollection<List<string>>(this.data);
        }
    }
}
