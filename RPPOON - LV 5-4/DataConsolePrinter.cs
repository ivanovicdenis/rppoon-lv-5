﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace RPPOON___LV_5_4
{
    class DataConsolePrinter
    {
        ReadOnlyCollection<List<string>> data;
        public void PrintData(IDataset data)
        {
            this.data = data.GetData();

            Console.WriteLine("\n Ispis podataka: \n");
            if (this.data == null)
            {
                Console.WriteLine("Nemate pravo pristupa!!");
            }
            else
            {
                foreach (List<string> lines in this.data)
                {
                    Console.WriteLine();
                    foreach (string line in lines)
                    {
                        Console.Write(line + ", ");
                    }
                }
            }

        }
    }
}
