﻿using System;
using System.Threading;

namespace RPPOON___LV_5_4
{
    class Program
    {
        static void Main(string[] args)
        {
            DataConsolePrinter printer = new DataConsolePrinter();

            //Testiranje klase VirtualProxyDataset
            Console.WriteLine("\nTestiranje VirtualProxyDataset klase: ");
            IDataset dataSet = new VirtualProxyDataset("data.csv");

            for (int i =0; i<10;i++) 
            {
                printer.PrintData(dataSet);
                System.Threading.Thread.Sleep(1000);

            }
        }
    }
}
