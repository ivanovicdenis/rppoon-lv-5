﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_5_4
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;
        private string filePath;

        private ConsoleLogger(string filePath)
        {
            this.filePath = filePath;
        }
        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
            {
                instance = new ConsoleLogger("log.csv");
            }
            return instance;
        }
        public void Log(string log)
        {
            using (System.IO.StreamWriter writer =
            new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(log);
            }

        }
    }
}
