﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_5_7
{
    class Notebook
    {
        private ITheme notebookTheme;
        private List<Note> notes;
        public Notebook(ITheme theme) 
        { 
            this.notes = new List<Note>();
            notebookTheme = theme;
        }
        public void AddNote(Note note) 
        {
            note.ChangeTheme(notebookTheme);
            this.notes.Add(note); 
        }
        public void ChangeTheme(ITheme theme)
        {
            notebookTheme = theme;
            foreach (Note note in this.notes)
            {
                note.Theme = notebookTheme;
            }
        }
        public void Display()
        {
            foreach (Note note in this.notes)
            {
                note.Show();
                Console.WriteLine("\n");
            }
        }
    }
}
