﻿using System;

namespace RPPOON___LV_5_7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Prva grupa
            GroupNote officeStaff = new GroupNote("Napraviti narativno izvješće svih odrađenih projekata za 2019. godinu", new BlueTheme());
            officeStaff.AddMember("Marko");
            officeStaff.AddMember("Luka");
            officeStaff.AddMember("Ivan");

            //Druga grupa
            GroupNote saleManagers = new GroupNote("Napraviti specijalne akcije za BlackFriday do maksimalno 90% popusta na maržu pojedinog proizvoda", new LightTheme());
            saleManagers.AddMember("Josip");
            saleManagers.AddMember("Marija");
            saleManagers.AddMember("Katarina");


            Notebook allGroupNotes = new Notebook(new BlueTheme());
            allGroupNotes.AddNote(officeStaff);
            allGroupNotes.AddNote(saleManagers);
            allGroupNotes.Display();
            allGroupNotes.ChangeTheme(new LightTheme());
            allGroupNotes.Display();
        }
    }
}
