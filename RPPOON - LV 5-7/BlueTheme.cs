﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_5_7
{
    class BlueTheme: ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Gray;
        }
        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
        }
        public string GetHeader(int width)
        {
            return new string('+', width);
        }
        public string GetFooter(int width)
        {
            return new string('x', width);
        }
    }
}
