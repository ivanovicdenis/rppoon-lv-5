﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RPPOON___LV_5_7
{
    class GroupNote:Note
    {
        List<string> members;
        public GroupNote(string message, ITheme theme) : 
            base(message, theme) 
        {
            members = new List<string>();
        }

        public override void Show()
        {
                this.ChangeColor();
                Console.WriteLine("\nGROUP NOTE: ");
                string framedMessage = this.GetFramedMessage();
                Console.WriteLine(framedMessage);
                Console.WriteLine("\nGroup members:  ");
                foreach (string member in members)
                {
                    Console.Write("||" + member + "||");
                }
                Console.ResetColor();
        }
        public void AddMember(string member)
        {
            if (!members.Contains(member))
            {
                members.Add(member);
            }
        }
        public void RemoveMember(string member)
        {
            members.Remove(member);
        }
    }
}
