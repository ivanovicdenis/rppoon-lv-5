﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_5
{
    class ShippingService
    {
        public double pricePerKilo { get; }

        public ShippingService(double pricePerKilo)
        {
            this.pricePerKilo = pricePerKilo;
        }
        public double CalculateDeliveryPrice(Box box)
        {
            return pricePerKilo * box.Weight;
        }
    }
}
