﻿using System;

namespace RPPOON___LV_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box_4453 = new Box("ivanovicdenis");
            box_4453.Add(new Product("Zvučnik", 90.54, 10));
            box_4453.Add(new Product("Laptop", 980.84, 5.9));
            box_4453.Add(new Product("Kalkulator", 24, 0.150));
            ShippingService shippingService = new ShippingService(5.45);
            Console.WriteLine("Ukupna masa paketa: " + box_4453.Weight);
            Console.WriteLine("Ukupna vrijednost paketa: " + box_4453.Price);
            Console.WriteLine("Cijena dostave po kilogramu mase paketa: " + shippingService.pricePerKilo);
            Console.WriteLine("Cijena dostave: " + shippingService.CalculateDeliveryPrice(box_4453));
        }
    }
}
