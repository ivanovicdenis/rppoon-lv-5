﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_5_6
{
    interface ITheme
    {
        void SetBackgroundColor();
        void SetFontColor();
        string GetHeader(int width);
        string GetFooter(int width);
    }
}
