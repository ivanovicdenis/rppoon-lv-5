﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RPPOON___LV_5_3
{
    class Program
    {
        static void Main(string[] args)
        {
            DataConsolePrinter printer = new DataConsolePrinter();

            //Testiranje klase VirtualProxyDataset
            Console.WriteLine("\nTestiranje VirtualProxyDataset klase: ");
            IDataset dataSet = new VirtualProxyDataset("data.csv");
            printer.PrintData(dataSet);

            //Testiranje klase ProtectionProxyDataset
            IDataset protectedDataSet;
            List<string> testUsersnames = new List<string>(new string[] { "test1", "test2", "test3" });
            List<User> testUsers = new List<User>();

            foreach (string username in testUsersnames)
            {
                testUsers.Add(User.GenerateUser(username));
            }

            Console.WriteLine("\nTestiranje ProtectionProxyDataset klase: ");
            foreach (User user in testUsers)
            {
                protectedDataSet = new ProtectionProxyDataset(user);
                printer.PrintData(protectedDataSet);
            }
        }
    }
}
