﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace RPPOON___LV_5_3
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
