﻿using System;

namespace RPPOON___LV_5_5
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme theme = new BlueTheme();
            ReminderNote reminderNote = new ReminderNote("Test", theme);
            reminderNote.Show();
        }
    }
}
